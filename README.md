#toggle-touchpad

This is a simple bash script that allow you to toggle your touchpad with
xinput utility.

To make it work get name of your device with

```
xinput list
```

Then change name in script from "SynPS/2 Synaptics TouchPad" to name of your
device.
